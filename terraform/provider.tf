terraform {
    required_providers {
    aws = {
        source  = "hashicorp/aws"
        version = "~> 3.27"
    }
    }


    backend "s3" {
        bucket = "test1414-terraform-s3-bucket"
        key    = "terraform.tfstate"
        region     = "us-east-1"
        dynamodb_table = "state-locking"
        encrypt = true
        access_key = ""
        secret_key = ""
    }
}
# Configure and downloading plugins for aws
provider "aws" {
  region     = "${var.aws_region}"
}
